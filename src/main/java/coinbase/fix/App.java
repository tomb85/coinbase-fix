package coinbase.fix;

import quickfix.*;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

public class App {

    public static void main(String[] args) throws Exception {

        final var settings = new SessionSettings("quickfix.settings");
        final var storeFactory = new NoopStoreFactory();
        final var logFactory = new ScreenLogFactory(settings);
        final var messageFactory = new quickfix.fix42.MessageFactory();
        final var logon = new CompletableFuture<Session>();
        final var initiator = new SocketInitiator(new Maker(logon), storeFactory, settings, logFactory, messageFactory);
        initiator.start();

        logon.whenComplete((session, throwable) -> {
            if (throwable == null) {
                System.out.println(session);
            } else {
                throwable.printStackTrace();
            }
        }).orTimeout(10, TimeUnit.SECONDS).join();

        initiator.stop(true);
    }
}
package coinbase.fix;

import static java.lang.System.getenv;

public class Configuration {

    private static final String COINBASE_PASSPHRASE_ENV_NAME = "COINBASE_PASSPHRASE";
    private static final String COINBASE_SECRET_ENV_NAME = "COINBASE_SECRET";
    private static final String COINBASE_KEY_ENV_NAME = "COINBASE_KEY";

    public static String passphrase() {
        return getenv(COINBASE_PASSPHRASE_ENV_NAME);
    }

    public static String secret() {
        return getenv(COINBASE_SECRET_ENV_NAME);
    }

    public static String key() {
        return getenv(COINBASE_KEY_ENV_NAME);
    }
}
package coinbase.fix;

import quickfix.*;
import quickfix.field.*;
import quickfix.fix42.Logon;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Base64;
import java.util.concurrent.CompletableFuture;

import static java.nio.charset.StandardCharsets.US_ASCII;

public class Maker extends ApplicationAdapter {

    private final String passphrase = Configuration.passphrase();
    private final String secret = Configuration.secret();

    private final CompletableFuture<Session> logon;

    public Maker(CompletableFuture<Session> logon) {
        this.logon = logon;
    }

    @Override
    public void toAdmin(Message message,
                        SessionID sessionId) {
        if (message instanceof Logon) {
            sign((Logon) message);
        }
    }

    private void sign(Logon logon) {
        try {
            logon.setField(new EncryptMethod(EncryptMethod.NONE_OTHER));
            logon.setField(new CharField(8013, 'S')); // Cancel on disconnect
            logon.setField(new Password(passphrase));
            logon.setField(new CharField(9406, 'N')); // Drop copy
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd-HH:mm:ss.SSS");
            String presign = String.join("\u0001", Arrays.asList(
                    logon.getHeader().getField(new SendingTime()).getValue().format(dateTimeFormatter),
                    Logon.MSGTYPE,
                    Integer.toString(logon.getHeader().getField(new MsgSeqNum()).getValue()),
                    logon.getHeader().getField(new SenderCompID()).getValue(),
                    logon.getHeader().getField(new TargetCompID()).getValue(),
                    passphrase
            ));
            Key key = new SecretKeySpec(Base64.getDecoder().decode(secret), "HmacSHA256");
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(key);
            mac.update(presign.getBytes(US_ASCII));
            String sign = Base64.getEncoder().encodeToString(mac.doFinal());
            logon.setField(new RawData(sign));
        } catch (Exception e) {
            this.logon.completeExceptionally(new RuntimeException("Unable to sign login message", e));
        }
    }

    @Override
    public void onLogon(SessionID sessionId) {
        logon.complete(Session.lookupSession(sessionId));
    }
}

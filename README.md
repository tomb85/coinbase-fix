# Overview

Connect and login to Coinbase FIX session and exit immediately.

# Building

```
$ export COINBASE_KEY=${YOUR_API_KEY}
$ ./gradlew clean build
```

# Running
```
$ export COINBASE_SECRET=${YOUR_API_SECRET}
$ export COINBASE_PASSPHRASE=${YOUR_API_PASSPHRASE}
$ ./gradlew run
```